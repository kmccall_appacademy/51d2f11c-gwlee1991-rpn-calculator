class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack.push(num)
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def times
    perform_operation(:*)
  end

  def divide
    perform_operation(:/)
  end

  def tokens(string)
    tokens = string.split
    tokens.map {|char| operation?(char) ? char.to_sym : Integer(char)}
  end

  def evaluate(string)
    tokens = tokens(string)

    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end

    value
  end

  private

  def perform_operation(symbol)
    raise "calculator is empty" if @stack.size < 2

    second = @stack.pop
    first = @stack.pop

    case symbol
    when :+

      @stack.push(first + second)
    when :-
      @stack.push(first - second)
    when :*
      @stack.push(first * second)
    when :/
      @stack.push(first.fdiv(second))
    else
      @stack.push(first)
      @stack.push(second)
      raise "No such operation :#{symbol}"
    end

  end

  def operation?(char)
    [:+, :-, :*, :/].include?(char.to_sym)
  end
end
